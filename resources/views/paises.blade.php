<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>
<body>
    <h1> Lista de paises </h1>
    <div class="table-responsive-xl">
    <table class="table table-hover">
        <thead class="thead-dark">
         <tr>
        <th scope="col">Pais</th>
        <th scope="col">Capital</th>
        <th scope="col">Moneda</th>
        <th scope="col">Población</th>
        <th scope="col">Ciudades Principales</th>
         </tr> 
        <tbody>
            <!--recorro la tabla foreach blade -->
            @foreach($paises as $pais => $infopais)
                <tr>
                   
                    <td rowspan="3"> {{ $pais }} </td>
                    <td rowspan="3"> {{ $infopais["Capital"] }}  </td>
                    <td rowspan="3"> {{ $infopais["Moneda"] }}  </td>
                    <td rowspan="3"> {{ $infopais["Población"] }}  </td>
                    <td > {{ $infopais["Ciudades"][0] }}  </td>
                </tr>
                <tr>
                    <td > {{ $infopais["Ciudades"][1] }}  </td>
                </tr>
                <tr>
                    <td > {{ $infopais["Ciudades"][2] }}  </td>
                </tr>
            @endforeach
        </tbody>
    </table>     
    </div>           
    
</body>
</html>