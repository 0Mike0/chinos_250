<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Ruta de prueba
Route::get('hola', function(){
    echo "Hola";
});
//Ruta arreglo
Route::get('arreglo',function(){

    //definido un arreglo
    $estudiantes = [ "A"=> "Ana", "M"=> "Maria", "V"=> "Valeria", "C"=> "Carlos"];
    echo "<pre>";
    var_dump( $estudiantes );
    echo "</pre>";

    //ciclos foreach: recorrer arreglo
    foreach($estudiantes as $indice => $e ){
        echo " $e  Tiene el indice '$indice' <br />";
    }
});

//Ruta de paises
Route::get('paises',function(){

    $paises=[
        "Colombia" => ["Capital" => "Bogotá",
                        "Moneda" => "Peso",
                        "Población" => 50372424,
                        "Ciudades" => ["Medellin" , "Cali", "Barranquilla"]
        ],
        "Perú" => ["Capital" => "Lima",
                    "Moneda" => "Sol",
                    "Población" => 33050325,
                    "Ciudades" => ["Cuzco","Trujillo","Iquitos"]
        ],
        "Ecuador" => ["Capital" => "Quito",
                    "Moneda" => "Dolar",
                    "Población" => 17517141,
                    "Ciudades" => ["Guayaquil","Cuenca","Salinas"]
        ],
        "Brazil" => ["Capital" => "Brasilia",
                    "Moneda" => "Real",
                    "Población" => 212216052,
                    "Ciudades" => ["Recife", "Rio de Janeiro","Salvador"]
        ],
        "España" => ["Capital" => "Madrid",
                    "Moneda" => "Euro",
                    "Población" => 45661564,
                    "Ciudades" => ["Barcelona" , "Sevilla", "Valencia"]
        ]
    ];

    //Enviar Datos de Paises a una Vista
    //Con la funcion view muestra la vista en el navegdor 
    return view('paises')->with("paises",$paises );

});